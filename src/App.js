import React, { Component } from "react";
import './App.css';

class App extends Component {
  
  render() {
    return (
      <main className="container">
        <h1 className='bg-warning text-primary text-center'>Les bon films de la mère Dudu</h1>
      </main>
    );
  }
}

export default App;