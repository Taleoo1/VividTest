import React, { Component } from 'react';
import { getMovies } from '../services/fakeMovieService';

class Movies extends Component {
    state = {
        movies : getMovies()
        }
        handleDelete = (movie) => {
            const movies = this.state.movies.filter(m => m.id !== movie.id);
            this.setState({movies})
        }
    render() {
        const {length: count} = this.state.movies;
        if (count === 0){
            return <p className='text-center'>There are no movies in the database</p>
        }
        return (
            <React.Fragment>
                <p className='text-center'>Visible {count} movies in the database</p>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Genre</th>
                            <th>Stock</th>
                            <th>Rate</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.movies.map((movie) => (
                        <tr key={movie.id}>
                            <td>{movie.title}</td>
                            <td>{movie.genre.name}</td>
                            <td>{movie.numberInStock}</td>
                            <td>{movie.dailyRentalRate}</td>
                            <th><button className='btn btn-danger' onClick={() => this.handleDelete(movie)}>Delete</button></th>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </React.Fragment>

        );
    }
}

export default Movies;